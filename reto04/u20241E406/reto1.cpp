#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

const int randnum(int min, int max) {
    return min + rand() % (max - min + 1);
}

void arreglo() {

    int tamao = randnum(100, 500);
    int* arreglo = new int[tamao];

    cout << "El tamano del arreglo es: " << tamao << endl;
    cout << "Los elementos del arreglo son:" << endl;

    for (int i = 0; i < tamao; ++i) {
        arreglo[i] = randnum(1, 10000);
        cout << arreglo[i] << " ";
    }
    cout << endl;

    delete[] arreglo;
}


int main() {
    srand(time(0));
    arreglo();
    system("pause");

    return 0;
}