#include <iostream>
#include <ctime>
using namespace std;

int *generar(int& n);

int main() {
    int n;
    int *arreglo;
    arreglo = generar(n);
    
    for (int i = 0; i < n; ++i) {
        cout << arreglo[i] << " ";
    }
    
    delete[]arreglo;
    system("pause");
}

int *generar(int& n) {
    n = rand() % 400 + 100;
    int *arreglo = new int[n];
    
    for (int i = 0; i < n; ++i) {
        arreglo[i] = rand() % 10000;
    }
    return arreglo;
}