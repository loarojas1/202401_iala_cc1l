#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

int **IniciarMatriz(int n, int m);
void ImprimirMatriz(int **Matriz, int n, int m);

int main() {

	int n;
	int m;
	int** Matriz;

	do {
		cout << "Ingrese un valor 'n' para generar una matriz n x m: " << endl;
		cin >> n;
	} while (n < 0);

	do {
		cout << "Ingrese un valor 'm' para generar una matriz n x m: " << endl;
		cin >> m;
	} while (m < 0);

	Matriz = IniciarMatriz(n, m);

	ImprimirMatriz(Matriz, n, m);

	for (int i = 0; i < m; ++i) {
		delete[]Matriz[i];
	}
	delete[]Matriz;

	system("pause");
	return EXIT_SUCCESS;
}

int** IniciarMatriz(int n, int m) {

	int** Matriz = new int*[m];
	for (int i = 0; i < m; ++i) {
		Matriz[i] = new int[n];
	}
	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			Matriz[i][j] = 0;
		}
	}
	return Matriz;
}
void ImprimirMatriz(int** Matriz, int n, int m) {
	for (int i = 0; i < m; ++i) {
		for (int j = 0; j < n; ++j) {
			cout << Matriz[i][j] << " ";
		}
		cout << endl;
	}
}